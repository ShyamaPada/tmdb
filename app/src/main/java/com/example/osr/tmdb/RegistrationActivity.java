package com.example.osr.tmdb;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by osr on 14/12/17.
 */

public class RegistrationActivity extends AppCompatActivity {
    EditText mEditTextName,mEditTextMobile,mEditTextPassword;
    EditText mEditTextDOB;
    Button mButtonRegister;//mButtonDatePicker;
    Boolean passMatching;
    TextView mTextViewLogin;
    private int mYear,mMonth,mDay;
    Calendar mCurrentDate;
    DatePickerDialog mDatePickerDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mEditTextName=findViewById(R.id.regName);
        mEditTextMobile=findViewById(R.id.regPhone);
        mEditTextPassword=findViewById(R.id.regPass);
        mEditTextDOB=findViewById(R.id.dob);
        mButtonRegister=findViewById(R.id.button_register);
        mTextViewLogin=findViewById(R.id.textView_Login) ;
        //mButtonDatePicker=findViewById(R.id.button_date_pick);
       mEditTextDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentDate=Calendar.getInstance();
                mYear=mCurrentDate.get(Calendar.YEAR);
                mMonth=mCurrentDate.get(Calendar.MONTH);
                mDay=mCurrentDate.get(Calendar.DAY_OF_MONTH);
                mMonth+=1;
                mEditTextDOB.setText(mYear+"-"+mMonth+"-"+mDay);
                mDatePickerDialog = new DatePickerDialog(RegistrationActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {


                                mEditTextDOB.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                mDatePickerDialog.show();
            }


        });

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences=getApplicationContext().getSharedPreferences
                        ("MyDBMS",MODE_PRIVATE);
                String userName=mEditTextName.getText().toString();
                String userMobile=mEditTextMobile.getText().toString();
                String pass=mEditTextPassword.getText().toString();
                String daten=mEditTextDOB.getText().toString();
                if(userMobile.isEmpty()||pass.isEmpty()||daten.isEmpty()){

                    mEditTextMobile.setText("");
                    mEditTextPassword.setText("");
                    mEditTextDOB.setText("");
                    Toast.makeText(getApplicationContext(),"Please fill all the details",
                            Toast.LENGTH_LONG).show();
                }
                else{
                    if(daten.charAt(4)=='-'){
                        daten=daten.substring(0,4);}
                    int date = Integer.valueOf(daten);
                    Calendar today=Calendar.getInstance();
                    int yourAge = today.get(Calendar.YEAR) - date;
                    SharedPreferences.Editor editor =sharedPreferences.edit();
                    editor.putString("name",userName);
                    editor.putString("mobile",userMobile);
                    editor.putString("password",pass);
                    editor.putInt("dateofbirth",date);
                    editor.commit();
                    if(userMobile.length()==10) {
                        if (pass.length() >= 4 && pass.length() <= 8) {

                            passMatching=PatternMatch.isValidPassword(pass);

                            if (passMatching==true)
                            {


                                if (yourAge> 18) {
                                    Intent intent = new Intent(getApplication(),
                                            LoginActivity.class);
                                    Toast.makeText(getApplicationContext(),"Registration " +
                                            "Succesfull",Toast.LENGTH_LONG).show();
                                    startActivity(intent);
                                    finish();
                                }
                                else{
                                    mEditTextDOB.setText("");
                                    Toast.makeText(getApplicationContext(),"Age more then 18 are" +
                                            " required",Toast.LENGTH_LONG).show();

                                }
                            }
                            else{

                                mEditTextPassword.setText("");
                                Toast.makeText(getApplicationContext(),"password are not match " +
                                        "with guideline",Toast.LENGTH_LONG).show();
                            }
                        }
                        else{

                            mEditTextPassword.setText("");
                            Toast.makeText(getApplicationContext(),"password length range 4 to 8",
                                    Toast.LENGTH_LONG).show();

                        }


                    }
                    else{

                        mEditTextMobile.setText("");
                        Toast.makeText(getApplicationContext(),"Error:Please fill again:Mobile " +
                                        "Number incorrect",
                                Toast.LENGTH_LONG).show();
                    }
                }}}

        );
        SpannableString spannableString=new SpannableString("*If you have already account " +
                "->Login");
        ClickableSpan clickableSpan=new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();

            }

        };
        spannableString.setSpan(clickableSpan,31,36,0);
        spannableString.setSpan(new ForegroundColorSpan(Color.GREEN),31,36,0);
        mTextViewLogin.setMovementMethod(LinkMovementMethod.getInstance());
        mTextViewLogin.setText(spannableString);
    }
}
