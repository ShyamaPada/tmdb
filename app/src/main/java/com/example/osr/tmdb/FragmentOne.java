package com.example.osr.tmdb;

import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by osr on 19/12/17.
 */

public class FragmentOne extends Fragment {

    View mView;
    List mMovieList = new ArrayList<>();
    RecyclerView mRecyclerView;
    MovieAdapter mMovieAdapter;
    ProgressBar mProgressBar;
    Integer page=1,pagination=0;
    //RecyclerView.LayoutManager mLayoutManager;
    LinearLayoutManager mLayoutManager;
    int mVisibleItemCount,mTotalItemCount,mPastVisiblesItems;
    Boolean loading=true,mNetWorkStateCheck=true;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView=inflater.inflate(R.layout.fragmentone, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mRecyclerView = mView.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        //mRecyclerView.setLayoutManager(mLayoutManager);
        mProgressBar = (ProgressBar)mView.findViewById(R.id.progressbar);
        mProgressBar.setVisibility(View.VISIBLE);
        if(this.getResources().getConfiguration().orientation==
                Configuration.ORIENTATION_PORTRAIT){
            mLayoutManager=new LinearLayoutManager(getActivity());}
        else{
            mLayoutManager=new LinearLayoutManager(getActivity());
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(mNetWorkStateCheck){
        new GetJSONData().execute();
            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy>0) {
                    mVisibleItemCount = mLayoutManager.getChildCount();
                    mTotalItemCount = mLayoutManager.getItemCount();
                    mPastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((mVisibleItemCount + mPastVisiblesItems) == mTotalItemCount) {
                            Toast.makeText(getActivity(), "Loading", Toast.LENGTH_LONG).show();
                           mProgressBar.setVisibility(View.VISIBLE);
                            if (mNetWorkStateCheck) {
                                page = page + 1;
                                pagination=1;
                                new GetJSONData().execute();
                            }
                        }
                    }

                }
            }
        });
    }}
    private class GetJSONData extends AsyncTask<Void, Void, Integer> {

        @Override
        protected Integer doInBackground(Void... voids) {
            String urlLink = "http://api.themoviedb.org/3/discover/movie?" +
                    "api_key=a12db14a03b602d933b97e62624902ad&page="+page;
            HttpURLConnection httpsURLConnection;
            Integer result = 0;

            try {
                URL url = new URL(urlLink);
                httpsURLConnection = (HttpURLConnection) url.openConnection();
                int statusCode = httpsURLConnection.getResponseCode();
                if (statusCode == 200) {
                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(httpsURLConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    parseJSONData(stringBuilder.toString());
                    result = 1;
                } else {
                    result = 0;
                }

            } catch (MalformedURLException e) {
                Log.d("MalformedURLException", e.getLocalizedMessage());
            } catch (IOException e) {
                Log.d("IOException", e.getLocalizedMessage());
                //e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer integer) {

            if (integer == 1) {
               mMovieAdapter = new MovieAdapter(getActivity(),mMovieList);
               mRecyclerView.setItemAnimator(new DefaultItemAnimator());
               mRecyclerView.setAdapter(mMovieAdapter);
               mProgressBar.setVisibility(View.INVISIBLE);
            } else
                Toast.makeText(getActivity(), "Failed to load JSON data",
                        Toast.LENGTH_LONG).show();
        }

        private void parseJSONData(String s) {

            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.optJSONArray("results");
               mMovieList = new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject post = jsonArray.getJSONObject(i);
                    Movie movie = new Movie();
                    movie.setmStringMovieName(post.optString("original_title"));
                    movie.setmStringMovieDescription(post.optString("overview"));
                    movie.setmStringMovieReleaseDate(post.optString("release_date"));
                    movie.setmStringMovieRating(post.optString("vote_average"));
                    movie.setmStringMoviePicURL(post.optString("poster_path"));
                    movie.setmStringMovieID(post.optString("id"));
                    movie.setmMoviePopularity(post.optDouble("popularity"));
                    mMovieList.add(movie);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
