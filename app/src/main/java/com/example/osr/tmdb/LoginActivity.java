package com.example.osr.tmdb;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by osr on 14/12/17.
 */

public class LoginActivity extends AppCompatActivity {
    EditText mEditTextMobile, mEditTextPassword;
    Button mButtonLogin;
    TextView mTextViewRegister;

    //SharedPreferences sharedPreferences;
    //SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEditTextMobile = findViewById(R.id.mobile);
        mEditTextPassword = findViewById(R.id.password);
        mButtonLogin = findViewById(R.id.login_button);
        mTextViewRegister=findViewById(R.id.textView_registration);
        //spannablestring
        SpannableString spannableString=new SpannableString("You haven't account Register");
        ClickableSpan clickableSpan=new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(intent);
                finish();
            }
        };
        spannableString.setSpan(clickableSpan,20,28,0);
        spannableString.setSpan(new ForegroundColorSpan(Color.BLUE),20,28,0);

        mTextViewRegister.setMovementMethod(LinkMovementMethod.getInstance());
        mTextViewRegister.setText(spannableString);
        //sharedPreferences.Editor=sharedPreferences.edit();
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobileNumber = mEditTextMobile.getText().toString();
                String pass = mEditTextPassword.getText().toString();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences
                        ("MyDBMS", MODE_PRIVATE);
                String userMobile = sharedPreferences.getString("mobile", "");
                String userPassword = sharedPreferences.getString("password", "");
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.commit();
                if (userMobile.matches(mobileNumber)) {
                    if (userPassword.matches(pass)) {
                        editor.putBoolean("Session", true);
                        editor.commit();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        mEditTextPassword.setText("");
                        Toast.makeText(getApplicationContext(), "Error:password not matched",
                                Toast.LENGTH_LONG).show();

                    }
                } else {
                    mEditTextMobile.setText("");
                    mEditTextPassword.setText("");
                    Toast.makeText(getApplicationContext(), "Error:mobile number and password " +
                            "are not matched", Toast.LENGTH_LONG).show();
                }

            }
        });

    }
}
