package com.example.osr.tmdb;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import io.netopen.hotbitmapgg.library.view.RingProgressBar;


/**
 * Created by osr on 19/12/17.
 */

public class DescriptionActivity extends AppCompatActivity {

    TextView mTextViewMovieName,mTextViewMovieDescription,mTextViewReleaseDate;
    ImageView mImageViewMoviePic;
    //Button mButtonLogout;
    //ImageView mImageViewBackdropPath;
    ImageButton mImageButonBack;
    String mMovieName,mMoviePicUrl,mMovieDescription,mMovieReleaseDate,mMovieRating;
    double mMoviePopularity;
    RingProgressBar mRingProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.description);
        mTextViewMovieName=findViewById(R.id.textView_title);
        mImageViewMoviePic=findViewById(R.id.imageView_poster);
        mTextViewMovieDescription=findViewById(R.id.textView_description);
        mTextViewReleaseDate=findViewById(R.id.textView_releasedate);
        mRingProgressBar=findViewById(R.id.ring_progress_bar);
        mRingProgressBar.setVisibility(View.VISIBLE);
        mImageButonBack=findViewById(R.id.imageButton_back);
        //mButtonLogout=findViewById(R.id.button_logout);
        mImageButonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DescriptionActivity.this,MainActivity.class);
                Toast.makeText(getApplicationContext(),"Back to movie List",Toast.LENGTH_SHORT).
                        show();
                startActivity(intent);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
            }
        });
       /* mButtonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences=getApplicationContext().
                        getSharedPreferences("MyDBMS",MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putBoolean("Session",false);
                editor.commit();
                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });*/
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(!bundle.isEmpty()){
            mMovieName = bundle.getString("name_movie");
            mMoviePicUrl = bundle.getString("pic_url_movie");
            mMovieDescription=bundle.getString("description_movie");
            mMovieReleaseDate=bundle.getString("release_date_movie");
            mMovieRating=bundle.getString("rating");
            mMoviePopularity=bundle.getDouble("popularity");
            int popularity = ((int)mMoviePopularity);
            mRingProgressBar.setProgress(popularity/10);
            mRingProgressBar.setVisibility(View.VISIBLE);
            mTextViewMovieName.setText(mMovieName);
            mTextViewReleaseDate.setText(mMovieReleaseDate);//...
            mTextViewMovieDescription.setText(mMovieDescription);
            Picasso.with(getApplicationContext())
                    .load("https://image.tmdb.org/t/p/w500/" + mMoviePicUrl)
                    .into(mImageViewMoviePic);



        } else
            Toast.makeText(DescriptionActivity.this, "Activity Data passing Error",
                    Toast.LENGTH_LONG).show();

    }


}
