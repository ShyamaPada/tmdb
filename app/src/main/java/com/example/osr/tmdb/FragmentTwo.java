package com.example.osr.tmdb;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;


/**
 * Created by osr on 19/12/17.
 */

public class FragmentTwo extends Fragment{
    View mView;
    MapView mMapView;
    GoogleMap mGoogleMap;
    FloatingActionButton mFloatingActionButton;
    Button mButtonSearch;
    int PLACE_PICKER_REQUEST=1;
    private TextView mTextView_Get_Place;
    SearchView mSearchView;
    EditText mEditTextSearch;
    //Marker markerOptions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mView=inflater.inflate(R.layout.fragmenttwo, container, false);
        mFloatingActionButton=mView.findViewById(R.id.floating_button);
        mButtonSearch=mView.findViewById(R.id.button_search);
        mMapView = (MapView) mView.findViewById(R.id.mapView);
        mEditTextSearch=mView.findViewById(R.id.edit_text_search);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately

    try {
        MapsInitializer.initialize(getActivity().getApplicationContext());
    } catch (Exception e) {
        e.printStackTrace();
    }

    mGoogleMap = mMapView.getMap();

    // latitude and longitude
    double latitude = 28.62196;
    double longitude = 77.38308;

    // create marker
    MarkerOptions marker = new MarkerOptions().position(
            new LatLng(latitude, longitude)).title("BrainMobi");

    // Changing marker icon
    marker.icon(BitmapDescriptorFactory
            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

    // adding marker
    mGoogleMap.addMarker(marker);
    CameraPosition cameraPosition = new CameraPosition.Builder()
            .target(new LatLng(28.62196, 77.38308)).zoom(12).build();
    mGoogleMap.animateCamera(CameraUpdateFactory
            .newCameraPosition(cameraPosition));

    mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                    mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(28.62196,
                            77.38308)).title("BrainMobi").icon(BitmapDescriptorFactory.
                            defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng
                            (28.62196, 77.38308), 12));



                }
            });


        }
    });
        /*mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng lat)
            {
                mGoogleMap.clear();
            }markerOptions.position(lat);
            markerOptions.title(lat.latitude+":"+lat.longitude);
            mGoogleMap.addMarker(markerOptions);
            //icon(BitmapDescriptorFactory.fromResource(R.drawable.loc)));

        });*/

        //mTextView_Get_Place=mView.findViewById(R.id.textView_get_place);
        //mSearchView=mView.findViewById(R.id.searchView);
        /*mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder=new PlacePicker.IntentBuilder();
                Intent intent;
                try{
                    intent=builder.build(getActivity());
                    startActivityForResult(intent,PLACE_PICKER_REQUEST);
                }catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });*/
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {


            @Override
            public void onMapClick(LatLng latLng) {
                mGoogleMap.clear();
                MarkerOptions  markerOptions=new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(latLng.latitude+":"+latLng.longitude);
                mGoogleMap.addMarker(markerOptions);



            }
        });
        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String location = mEditTextSearch.getText().toString();
                List addressList = null;
                Address address=null;
                if (location != null && !(location.equals("")))
                {
                    Geocoder geocoder = new Geocoder(getActivity().getApplicationContext());
                    try {
                        addressList = geocoder.getFromLocationName(location, 1);
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                    address = (android.location.Address) addressList.get(0);
                    LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                    Marker markerLocation= mGoogleMap.addMarker(new MarkerOptions().position(latLng).
                           title(location).icon(BitmapDescriptorFactory.
                            defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                   markerLocation.showInfoWindow();
                   CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).
                           zoom(12).build();
                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));}
                else{
                    Log.d("net",location);
                }
            }
        });



         return mView;


}

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==PLACE_PICKER_REQUEST){
            if(requestCode== Activity.RESULT_OK){
                Place place= PlacePicker.getPlace(data,getActivity());
                String address=String.format("Place",place.getAddress());
                mTextView_Get_Place.setText(address);

            }
        }
    }*/
}



