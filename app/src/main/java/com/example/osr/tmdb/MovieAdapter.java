package com.example.osr.tmdb;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by osr on 12/12/17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    List<Movie> mMovieList;
    Context mContext;



    public MovieAdapter(Context context, List<Movie> movieList) {
        this.mContext = context;
        this.mMovieList = movieList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,
                parent, false);

        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movie movie = mMovieList.get(position);
        holder.textViewMovieName.setText(movie.getmStringMovieName());
        holder.textViewMovieReleaseDate.setText(movie.getmStringMovieReleaseDate());
        holder.textViewMovieDescription.setText(movie.getmStringMovieDescription());
        holder.textViewMovieRating.setText(movie.getmStringMovieRating());
        Picasso.with(mContext)
                .load("https://image.tmdb.org/t/p/w500/" + movie.getmStringMoviePicURL())
                .into(holder.imageViewMoviePic);
    }

    @Override
    public int getItemCount() {
        return mMovieList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewMovieName, textViewMovieReleaseDate, textViewMovieDescription,
        textViewMovieRating;
        ImageView imageViewMoviePic;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewMovieName = itemView.findViewById(R.id.textView_movie_name);
            textViewMovieReleaseDate = itemView.findViewById(R.id.textView_movie_release_date);
            textViewMovieDescription = itemView.findViewById(R.id.textView_movie_description);
            imageViewMoviePic = itemView.findViewById(R.id.imageView_movie_image);
            textViewMovieRating=itemView.findViewById(R.id.textView_movie_rating);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    if (pos != RecyclerView.NO_POSITION) {

                        Movie clickedMovieItem = mMovieList.get(pos);
                        Toast.makeText(mContext, clickedMovieItem.getmStringMovieName(),
                         Toast.LENGTH_SHORT).show();
                        //Toast.makeText(context,"Movie description",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(mContext, DescriptionActivity.class);
                        intent.putExtra("name_movie",
                                clickedMovieItem.getmStringMovieName());
                        intent.putExtra("release_date_movie",
                                clickedMovieItem.getmStringMovieReleaseDate());
                        intent.putExtra("pic_url_movie",
                                clickedMovieItem.getmStringMoviePicURL());
                        intent.putExtra("description_movie",
                                clickedMovieItem.getmStringMovieDescription());
                        intent.putExtra("rating",clickedMovieItem.
                                getmStringMovieRating());
                        intent.putExtra("backdrop_path",clickedMovieItem.
                                getmStringMovieBackdoorPicURL());//new
                        intent.putExtra("popularity",clickedMovieItem.getmMoviePopularity());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);


                    }
                }
            });
        }
    }
}



