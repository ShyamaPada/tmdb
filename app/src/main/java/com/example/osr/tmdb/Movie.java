package com.example.osr.tmdb;

/**
 * Created by osr on 12/12/17.
 */

public class Movie {

    public Movie(){

    }

    private String mStringMovieName;
    private String mStringMovieReleaseDate;
    private String mStringMoviePicURL;
    private String mStringMovieDescription;
    private String mStringMovieID;
    private String mStringMovieRating;

    public String getmStringMovieBackdoorPicURL() {
        return mStringMovieBackdoorPicURL;
    }

    public void setmStringMovieBackdoorPicURL(String mStringMovieBackdoorPicURL) {
        this.mStringMovieBackdoorPicURL = mStringMovieBackdoorPicURL;
    }

    private String mStringMovieBackdoorPicURL;
    private double mMoviePopularity;

    public double getmMoviePopularity() {
        return mMoviePopularity;
    }

    public void setmMoviePopularity(Double mMoviePopularity) {
        this.mMoviePopularity = mMoviePopularity;
    }



    public String getmStringMovieName() {
        return mStringMovieName;
    }

    public void setmStringMovieName(String mStringMovieName) {
        this.mStringMovieName = mStringMovieName;
    }

    public String getmStringMovieReleaseDate() {
        return mStringMovieReleaseDate;
    }

    public void setmStringMovieReleaseDate(String mStringMovieReleaseDate) {
        this.mStringMovieReleaseDate = mStringMovieReleaseDate;
        this.mStringMovieReleaseDate=mStringMovieReleaseDate.substring(0,4);
    }

    public String getmStringMoviePicURL() {
        return mStringMoviePicURL;
    }

    public void setmStringMoviePicURL(String mStringMoviePicURL) {
        this.mStringMoviePicURL = mStringMoviePicURL;
    }

    public String getmStringMovieDescription() {
        return mStringMovieDescription;
    }

    public void setmStringMovieDescription(String mStringMovieDescription) {
        this.mStringMovieDescription = mStringMovieDescription;
    }

    public String getmStringMovieID() {
        return mStringMovieID;
    }

    public void setmStringMovieID(String mStringMovieID) {
        this.mStringMovieID = mStringMovieID;
    }

    public String getmStringMovieRating() {
        return mStringMovieRating;
    }

    public void setmStringMovieRating(String mStringMovieRating) {
        this.mStringMovieRating = mStringMovieRating;
    }


    /*public Movie(String mStringMovieName, String mStringMovieReleaseDate, String mStringMoviePicURL
            , String mStringMovieDescription, String mStringMovieID, String mStringMovieRating) {
        this.mStringMovieName = mStringMovieName;
        this.mStringMovieReleaseDate = mStringMovieReleaseDate;
        this.mStringMoviePicURL = mStringMoviePicURL;
        this.mStringMovieDescription = mStringMovieDescription;
        this.mStringMovieID = mStringMovieID;
        this.mStringMovieRating = mStringMovieRating;
    }*/

}
