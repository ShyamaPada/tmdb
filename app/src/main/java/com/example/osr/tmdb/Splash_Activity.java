package com.example.osr.tmdb;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Intent intent=new Intent(Splash_Activity.this,LoginActivity.class);
                //startActivity(intent);
                //finish();
                SharedPreferences sharedPreferences=getApplicationContext().
                        getSharedPreferences("MyDBMS",MODE_PRIVATE);
                if(sharedPreferences.getBoolean("Session",false)){
                    Intent intent =new Intent(Splash_Activity.this,MainActivity.class);
                    startActivity(intent);
                    finish();}
                else{
                    Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },1000);
    }
}
